<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Modal to upload Picture</h2>

    <!-- Trigger the modal with a button -->
    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">click Here</button>

    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"> Upload Picture</h4>
                </div>
                <div class="modal-body">
                    <form action="file.php" method="post" enctype="multipart/form-data">
                    Picture Description<br>
                        <input type="text" class="form-control" placeholder=" Picture description" name="Picture description">
                        <br>
                    </div>
                <div class="form-group">
                   Date:<br>
                    <input type="date" class="form-control" id="date" placeholder="Enter date" name="date">
                </div>
                   <div class="form-group">
                    File upload: <br>
                    <input type="file" class="form-control" id="file" placeholder="Choose File" name="FileToUpload">
                    <button type="submit" class="btn btn-primary">Submit</button>
                   </div>
                    </form>

                    <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">close</button>
                </div>
            </div>

        </div>
    </div>

</div>

</body>
</html>